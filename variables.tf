variable "project" {
  type = string
}

variable "zone" {
  type = string
}

variable "region" {
  type = string
}

variable "network" {
  type = string
}

variable "cluster_size" {
  default = 1
}

variable "cluster_name" {
  type = string
}

variable "ca_file" {
  type = string
}

variable "certificate_authority_remote_state_bucket" {
  type = string
}

variable "certificate_authority_remote_state_prefix" {
  type = string
}

variable "consul_datacenter" {
  type = string
}

variable "consul_auto_join_tag" {
  type = string
}

variable "machine_type" {
  type = string
}

variable "dns_managed_zone" {
  type = string
}
