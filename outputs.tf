# output "acl_token" {
#   value       = nomadutility_acl_bootstrap.init
#   description = "Bootstrap ACL"
#   sensitive   = true
# }

output "external_ips" {
  value = google_compute_address.public.*.address
}

output "external_endpoints" {
  value = formatlist("https://%s:4646", google_compute_address.public.*.address)
}
