resource "google_compute_instance" "nomad" {
  count = var.cluster_size

  name         = "${var.cluster_name}-${count.index}"
  machine_type = var.machine_type
  zone         = var.zone

  tags = [var.cluster_name]

  boot_disk {
    initialize_params {
      image = data.google_compute_image.nomad.self_link
    }
  }

  network_interface {
    network = var.network
    access_config {
      nat_ip = google_compute_address.public[count.index].address
    }
  }

  scheduling {
    preemptible       = true
    automatic_restart = false
  }

  service_account {
    scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }

  metadata = {
    "user-data" = templatefile("${path.module}/cloud-init.yaml", {
      nomad_ca_pem_b64  = base64encode(data.terraform_remote_state.ca.outputs.root_ca_certificate_pem)
      nomad_tls_key_b64 = base64encode(tls_private_key.nomad[count.index].private_key_pem)
      nomad_tls_crt_b64 = base64encode(join("\n", [
        tls_locally_signed_cert.nomad[count.index].cert_pem,
        data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem,
        data.terraform_remote_state.ca.outputs.root_ca_certificate_pem,
      ]))


      consul_ca_pem_b64  = base64encode(data.terraform_remote_state.ca.outputs.root_ca_certificate_pem)
      consul_tls_key_b64 = base64encode(tls_private_key.consul[count.index].private_key_pem)
      consul_tls_crt_b64 = base64encode(join("\n", [
        tls_locally_signed_cert.consul[count.index].cert_pem,
        data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem,
        data.terraform_remote_state.ca.outputs.root_ca_certificate_pem,
      ]))
      consul_gossip_b64 = base64encode(jsonencode({ encrypt = data.terraform_remote_state.consul.outputs.gossip_key }))
      retry_join_b64 = base64encode(jsonencode({
        retry_join = ["provider=gce project_name=${var.project} tag_value=${var.consul_auto_join_tag}"]
      }))
      datacenter_b64 = base64encode(jsonencode({
        datacenter = var.consul_datacenter
      }))
      acl_b64 = base64encode(templatefile("${path.module}/templates/consul-acl.hcl", {
        consul_acl_token = data.terraform_remote_state.consul.outputs.root_token
      }))

      tls_config_b64     = base64encode(file("${path.module}/templates/tls.hcl"))
      storage_config_b64 = base64encode(file("${path.module}/templates/storage.hcl"))
      consul_config_b64 = base64encode(templatefile("${path.module}/templates/consul.hcl", {
        cluster_size     = var.cluster_size
        consul_acl_token = data.terraform_remote_state.consul.outputs.root_token
      }))

      vault_config_b64 = base64encode(templatefile("${path.module}/templates/vault.hcl", {
        address = data.terraform_remote_state.vault.outputs.endpoints[0]
      }))

      nomad_env_b64 = base64encode(templatefile("${path.module}/templates/vault.env", {
        token = data.terraform_remote_state.vault.outputs.root_token
      }))
    })
  }
}
