acl {
  enabled = true

  tokens {
    default = "${consul_acl_token}"
  }
}
