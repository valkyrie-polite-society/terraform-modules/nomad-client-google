vault {
  enabled = true
  address = "${address}"
  ca_file = "/etc/nomad/ca.pem"
}
