# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/adriennecohea/nomadutility" {
  version     = "0.0.14"
  constraints = ">= 0.0.14"
  hashes = [
    "h1:2FLpOL7JeaSu1s+P02LQZu3VgrvqjmHRPPb6Fodx/ZA=",
    "zh:27747a1da7905ed7711e8c9c962809a9fc054ef3571fdb21d4638fa56543d13c",
    "zh:a0596f0198fdfa5250de0646fb92532fe573f7357edb416da1b09fe8aa42f973",
    "zh:a07a1e759b24477d30ac3e85515c7ed805cb7a9aae9662b40cd25440f9153970",
    "zh:dfa00cfa874d922ad9b38e9ec0f54cd8c44d766e6fda575487e5fa0a780ddcd3",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.40.0"
  constraints = "~> 3.40.0"
  hashes = [
    "h1:09F9Q62aeWgIVEuAqA9WafkvSzHdvDUCpFni0X3GVyY=",
    "zh:1034d628bfb6c751abce884a2e7035abd2e05a2f944818ead2bc53bdf8f33914",
    "zh:4285e594dde25ec948b4e5a73f727769d429758a7b4e8b9bcb66fdb51c68a73f",
    "zh:83ed507eebd4c05a3bb2508956bba68ac1336aa3a7778256d131aa388aaa905d",
    "zh:98fba5b49bf6a686cabc4fe5d187372849936bab279672bb3de466cf28823416",
    "zh:9e08989bc04a5e3ca0ebc2aca634f1a873105465532f361e1d3382a5b11d4fa5",
    "zh:b781f2bec0f9d7375fbdb769ed151c2ddff33231c148609a18d45b74eb0f7371",
    "zh:bf1432de90f1a856c7d895498cc40944bc13f58af5045bb43724595259cd1d11",
    "zh:c38ec5aa03aed5dae49062c30691722fa49e0c5ac1e730c635c69e927b0c6845",
    "zh:d4eb990842a95fef449a4bf6aec625268fa963fd1ebc793060acd0e10512628a",
    "zh:daac70787ad80c760e24da74b7b0f4b2db9414b92347a1ce95aa3a5a3e1261d3",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version = "3.1.0"
  hashes = [
    "h1:fUJX8Zxx38e2kBln+zWr1Tl41X+OuiE++REjrEyiOM4=",
    "zh:3d46616b41fea215566f4a957b6d3a1aa43f1f75c26776d72a98bdba79439db6",
    "zh:623a203817a6dafa86f1b4141b645159e07ec418c82fe40acd4d2a27543cbaa2",
    "zh:668217e78b210a6572e7b0ecb4134a6781cc4d738f4f5d09eb756085b082592e",
    "zh:95354df03710691773c8f50a32e31fca25f124b7f3d6078265fdf3c4e1384dca",
    "zh:9f97ab190380430d57392303e3f36f4f7835c74ea83276baa98d6b9a997c3698",
    "zh:a16f0bab665f8d933e95ca055b9c8d5707f1a0dd8c8ecca6c13091f40dc1e99d",
    "zh:be274d5008c24dc0d6540c19e22dbb31ee6bfdd0b2cddd4d97f3cd8a8d657841",
    "zh:d5faa9dce0a5fc9d26b2463cea5be35f8586ab75030e7fa4d4920cd73ee26989",
    "zh:e9b672210b7fb410780e7b429975adcc76dd557738ecc7c890ea18942eb321a5",
    "zh:eb1f8368573d2370605d6dbf60f9aaa5b64e55741d96b5fb026dbfe91de67c0d",
    "zh:fc1e12b713837b85daf6c3bb703d7795eaf1c5177aebae1afcf811dd7009f4b0",
  ]
}
