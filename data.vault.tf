data "terraform_remote_state" "vault" {
  backend = "gcs"

  config = {
    bucket = "adrienne-devops-terraform-states"
    prefix = "gitlab.com/adriennes-spells/raft-vault-pki-google"
  }
}
